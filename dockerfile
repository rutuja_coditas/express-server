FROM node:19-alpine3.16
COPY . app/
WORKDIR /app
RUN npm i
RUN npm i typescript
CMD  npm start
